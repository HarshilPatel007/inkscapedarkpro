# InkscapeDarkPro

Professional dark theme for Inkscape.

> Based on [AdwMod](https://gitlab.com/HarshilPatel007/AdwMod-theme)

> Icons _Minimal Icon By 7k_ [link to Icon Pack](https://inkscape.org/~7kv929/%E2%98%85minimal-icon-by-7k)



### dev notes. (not for end-users)
- clone repo
- `meson build -Dprefix="$HOME/.local" -Dcolors=dark -Dsizes=compact -Dtheme_name=InkscapeDarkPro`
- `ninja -C build`
